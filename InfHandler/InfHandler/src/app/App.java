package app;

import java.sql.SQLException;

import ui.MainWindow;

public class App {

	public static void main(String[] args) throws SQLException {
		MainWindow mw = new MainWindow();
		mw.init();
	}

}
