package delegate;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTable;

import ui.InfHandlerPopUp;

public class PopClickListener extends MouseAdapter {
	
	private String fileName;
	private JTable table;
	
    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public PopClickListener(String fileName) {
		 this.fileName = fileName;
	}
	
	public PopClickListener(JTable table) {
        this.table = table;
	}

	public void mousePressed(MouseEvent e) {
        if (e.isPopupTrigger())
            doPop(e);
    }

    public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger())
            doPop(e);
    }

    private void doPop(MouseEvent e) {
    	InfHandlerPopUp contextMenu = new InfHandlerPopUp();
    	contextMenu.show(e.getComponent(), e.getX(), e.getY());
    }
}
