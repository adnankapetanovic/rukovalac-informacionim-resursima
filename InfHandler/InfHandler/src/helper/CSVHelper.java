package helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public class CSVHelper {

	public static ArrayList<String> ucitajCSV(String path) {
		File file = new File(path);
		BufferedReader csvReader = null;
		ArrayList<String> deserijalizovaniPodaci = new ArrayList<String>();

		try {
			FileInputStream fis = new FileInputStream(file);
			csvReader = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
			String ucitano = csvReader.readLine();
			deserijalizovaniPodaci.add(ucitano);
			while(ucitano != null) {
				ucitano = csvReader.readLine();
				if (ucitano!=null) {
					deserijalizovaniPodaci.add(ucitano);
				}
			}
			fis.close();
			csvReader.close();
			} 
			catch (FileNotFoundException e) {
				System.out.println("Fajl nije pronadjen!");
			}
			catch (IOException e) {
				System.out.println("Greska prilikom ucitavanja podataka iz datoteke!");
			}
		return deserijalizovaniPodaci;
	}
	
	public static void upisiUCSV(String path, ArrayList<String> stringoviZaUpis) {
		//Ucitamo CSV u listu kako bi umetnuli izmenjen entitet
		ArrayList<String> backupList = ucitajCSV(path);
		//lociramo izmenjeni entitet i prepisemo ga
		try {
			File file = new File(path);
			FileWriter writer = new FileWriter(path);  
		    BufferedWriter buffer = new BufferedWriter(writer);
		    
		    for (int i = 0; i < stringoviZaUpis.size(); i++) {
		    	if (i< stringoviZaUpis.size()-1) {
		    		buffer.append(stringoviZaUpis.get(i));
			    	 buffer.newLine();
				}
		    	else {
		    		 buffer.append(stringoviZaUpis.get(i));
				}
		    }
		    buffer.flush();
		    buffer.close();  
		    //System.out.println("Success");  
			
			FileOutputStream fos = new  FileOutputStream(file, true);
			//fos.write(data.objekatUCSVFormat().getBytes());
			fos.write("\n".getBytes());
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fajl nije nadjen!");
		} catch (IOException e) {
			System.out.println("Doslo je do problema prilikom upisivanja podataka u datoteku!");
		}
	}
}
