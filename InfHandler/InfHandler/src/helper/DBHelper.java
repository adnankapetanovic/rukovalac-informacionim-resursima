package helper;

import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTable;

public class DBHelper {
	static String mysqlUrl = "jdbc:mysql://localhost/inf_handler";
	static String dbName = "root";
	static String dbPassword = "1234";
	Connection con = DriverManager.getConnection(mysqlUrl, dbName, dbPassword);
	
	public DBHelper() throws SQLException{
		super();
	}
	public ArrayList<String> getDBTablesList() throws SQLException {
		ArrayList<String> tablesList = new ArrayList<String>();
		//Registrovanje Drajvera
	      DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
	      //Dobavljanje konekcije
	      //System.out.println("Konekcija uspostaljena...");
	      //Kreiranje iskaza
	      Statement stmt = con.createStatement();
	      //Dobavljanje podataka
	      ResultSet rs = stmt.executeQuery("Show tables");
	      //System.out.println("Tabele u bazi: ");
	      while(rs.next()) { 
	    	  tablesList.add((rs.getString(1)));
	      }
	      con.close();
	      return tablesList;
	}
	
	public static JTable dBTableToJTable(String dbTableName) {
		Connection con;
		try {
			con = DriverManager.getConnection(mysqlUrl, "root", "1234");
		
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM " + dbTableName);
		ResultSetMetaData md = rs.getMetaData();
		int columns = md.getColumnCount();
		
		//lista naziva kolona
		ArrayList columnNames = new ArrayList();
		
		//lista sa podacima za kolonu
		ArrayList data = new ArrayList();
		
		//dobavi naziv kolona
		for (int i = 1; i <= columns; i++) {
		      columnNames.add(md.getColumnName(i));
		    }
		
		//dobavi vrednosti
		while (rs.next()) {
			ArrayList row = new ArrayList(columns);
			for (int i = 1; i <= columns; i++) {
				row.add(rs.getObject(i));
			}
			data.add(row);
		}
		
	    Vector columnNamesVector = new Vector();
	    Vector dataVector = new Vector();
	    for (int i = 0; i < data.size(); i++) {
	      ArrayList subArray = (ArrayList) data.get(i);
	      Vector subVector = new Vector();
	      for (int j = 0; j < subArray.size(); j++) {
	        subVector.add(subArray.get(j));
	      }
	      dataVector.add(subVector);
	    }
	    for (int i = 0; i < columnNames.size(); i++)
	      columnNamesVector.add(columnNames.get(i));
	    JTable table = new JTable(dataVector, columnNamesVector);
		con.close();
		return table;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
