package helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FileHelper {
	
	public static String fileNametoCamelCase(String stringToChange) {
		String[] words = stringToChange.split("[\\W_]+");
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			if (i == 0) {
				word = word.isEmpty() ? word : word.toLowerCase();
			} else {
				word = word.isEmpty() ? word : Character.toUpperCase(word.charAt(0)) + word.substring(1).toLowerCase();
			}
			builder.append(word);
		}
		return builder.toString();
	}
	
	public static String createPathByFileName(String path) throws FileNotFoundException, JSONException {
		//pronadji json
		String fileName  = path.substring(0, path.length() - 4);
		String adaptedJSONName = FileHelper.fileNametoCamelCase(fileName);
		File fileToSearchThrough = new File("data/Metaopisi/" + adaptedJSONName + ".json");
		String csvFilePath = null;
		
		//uzmi putanju
		InputStream inputStream = new FileInputStream(fileToSearchThrough);
		Scanner s = new Scanner(inputStream).useDelimiter("\\A");
		String jsonTxt = s.hasNext() ? s.next() : "";
		JSONObject json = new JSONObject(jsonTxt);
		String[] filepath = JSONObject.getNames(json);
		for (String string : filepath) {
			if (string.equals("file")) {
				csvFilePath = json.getString(string);
			}
		}
		return csvFilePath;
	}
	
	public static ArrayList<String> getChildTablePathList(String fileName) throws FileNotFoundException, JSONException {
		ArrayList<String> childData = new ArrayList<String>();
		String file = fileName.substring(0, fileName.length() - 4);
		File f = new File("data/Metaopisi");

		String adaptedJSONName = FileHelper.fileNametoCamelCase(file);
		File fileToSearchThrough = new File("data/Metaopisi/" + adaptedJSONName + ".json");

		if (f.exists()) {
			InputStream inputStream = new FileInputStream(fileToSearchThrough);
			Scanner s = new Scanner(inputStream).useDelimiter("\\A");
			String jsonTxt = s.hasNext() ? s.next() : "";
			JSONObject json = new JSONObject(jsonTxt);
			JSONObject relations = json.getJSONObject("relations");
			JSONArray to = relations.getJSONArray("to");
			for (int i = 0; i < to.length(); i++) {
				JSONObject objectInArray = to.getJSONObject(i);

				String[] elementNames = JSONObject.getNames(objectInArray);
				for (String elementName : elementNames) {
					String value = objectInArray.getString(elementName);
					if (elementName.equals("file")) {
						childData.add(value);
					}
				}
			}
		}
		return childData;
	}
}
