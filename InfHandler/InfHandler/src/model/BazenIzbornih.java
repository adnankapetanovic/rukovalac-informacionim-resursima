package model;

public class BazenIzbornih {
	private VisokoskolskaUstanova ustanova;
	private String izbornaPozicija;
	private double redniBroj;
	private String biraSePredmet;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public String getIzbornaPozicija() {
		return izbornaPozicija;
	}
	public void setIzbornaPozicija(String izbornaPozicija) {
		this.izbornaPozicija = izbornaPozicija;
	}
	public double getRedniBroj() {
		return redniBroj;
	}
	public void setRedniBroj(double redniBroj) {
		this.redniBroj = redniBroj;
	}
	public String getBiraSePredmet() {
		return biraSePredmet;
	}
	public void setBiraSePredmet(String biraSePredmet) {
		this.biraSePredmet = biraSePredmet;
	}
	
	public BazenIzbornih() {
		super();
	}
	
	public BazenIzbornih(VisokoskolskaUstanova ustanova, String izbornaPozicija, double redniBroj,
			String biraSePredmet) {
		super();
		this.ustanova = ustanova;
		this.izbornaPozicija = izbornaPozicija;
		this.redniBroj = redniBroj;
		this.biraSePredmet = biraSePredmet;
	}
}
