package model;

public class Drzava {
	private String oznaka;
	private String naziv;
	private int glavniGrad;
	private String pravniNaslednik;
	
	public String getOznaka() {
		return oznaka;
	}
	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getGlavniGrad() {
		return glavniGrad;
	}
	public void setGlavniGrad(int glavniGrad) {
		this.glavniGrad = glavniGrad;
	}
	public String getPravniNaslednik() {
		return pravniNaslednik;
	}
	public void setPravniNaslednik(String pravniNaslednik) {
		this.pravniNaslednik = pravniNaslednik;
	}
	
	public Drzava() {
		super();
	}
	
	public Drzava(String oznaka, String naziv, int glavniGrad, String pravniNaslednik) {
		super();
		this.oznaka = oznaka;
		this.naziv = naziv;
		this.glavniGrad = glavniGrad;
		this.pravniNaslednik = pravniNaslednik;
	}
}
