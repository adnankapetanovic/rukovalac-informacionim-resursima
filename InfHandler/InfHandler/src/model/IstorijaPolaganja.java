package model;

import java.time.LocalDate;

public class IstorijaPolaganja {
	private VisokoskolskaUstanova ustanova;
	private String brojIndeksa;
	private int brojPolaganja;
	private int polagaoKod;
	private int predmet;
	private LocalDate kada;
	private double bodova;
	private int ocena;
	private int ESPB;
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public String getBrojIndeksa() {
		return brojIndeksa;
	}
	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}
	public int getBrojPolaganja() {
		return brojPolaganja;
	}
	public void setBrojPolaganja(int brojPolaganja) {
		this.brojPolaganja = brojPolaganja;
	}
	public int getPolagaoKod() {
		return polagaoKod;
	}
	public void setPolagaoKod(int polagaoKod) {
		this.polagaoKod = polagaoKod;
	}
	public int getPredmet() {
		return predmet;
	}
	public void setPredmet(int predmet) {
		this.predmet = predmet;
	}
	public LocalDate getKada() {
		return kada;
	}
	public void setKada(LocalDate kada) {
		this.kada = kada;
	}
	public double getBodova() {
		return bodova;
	}
	public void setBodova(double bodova) {
		this.bodova = bodova;
	}
	public int getOcena() {
		return ocena;
	}
	public void setOcena(int ocena) {
		this.ocena = ocena;
	}
	public int getESPB() {
		return ESPB;
	}
	public void setESPB(int eSPB) {
		ESPB = eSPB;
	}
	
	public IstorijaPolaganja() {
		super();
	}
	
	public IstorijaPolaganja(VisokoskolskaUstanova ustanova, String brojIndeksa, int brojPolaganja, int polagaoKod,
			int predmet, LocalDate kada, double bodova, int ocena, int eSPB) {
		super();
		this.ustanova = ustanova;
		this.brojIndeksa = brojIndeksa;
		this.brojPolaganja = brojPolaganja;
		this.polagaoKod = polagaoKod;
		this.predmet = predmet;
		this.kada = kada;
		this.bodova = bodova;
		this.ocena = ocena;
		ESPB = eSPB;
	}
	
	
	}
