package model;

public class IzvodiNastavu {
	private VisokoskolskaUstanova ustanova;
	private int predmet;
	private int brojAktivnosti;
	private int nastavnik;
	private char vrsta;
	private boolean ispituje;
	private boolean aktuelan;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public int getPredmet() {
		return predmet;
	}
	public void setPredmet(int predmet) {
		this.predmet = predmet;
	}
	public int getBrojAktivnosti() {
		return brojAktivnosti;
	}
	public void setBrojAktivnosti(int brojAktivnosti) {
		this.brojAktivnosti = brojAktivnosti;
	}
	public int getNastavnik() {
		return nastavnik;
	}
	public void setNastavnik(int nastavnik) {
		this.nastavnik = nastavnik;
	}
	public char getVrsta() {
		return vrsta;
	}
	public void setVrsta(char vrsta) {
		this.vrsta = vrsta;
	}
	public boolean isIspituje() {
		return ispituje;
	}
	public void setIspituje(boolean ispituje) {
		this.ispituje = ispituje;
	}
	public boolean isAktuelan() {
		return aktuelan;
	}
	public void setAktuelan(boolean aktuelan) {
		this.aktuelan = aktuelan;
	}
	
	public IzvodiNastavu() {
		super();
	}
	
	public IzvodiNastavu(VisokoskolskaUstanova ustanova, int predmet, int brojAktivnosti, int nastavnik, char vrsta,
			boolean ispituje, boolean aktuelan) {
		super();
		this.ustanova = ustanova;
		this.predmet = predmet;
		this.brojAktivnosti = brojAktivnosti;
		this.nastavnik = nastavnik;
		this.vrsta = vrsta;
		this.ispituje = ispituje;
		this.aktuelan = aktuelan;
	}
}
