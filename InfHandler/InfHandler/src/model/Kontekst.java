package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.json.JSONException;

import helper.CSVHelper;
import helper.FileHelper;
import ui.WorkSpaceSplitPane;

public class Kontekst {
	protected ArrayList<String> listaKontekst = new  ArrayList<String>();
	protected static String adresaKonteksta  = "resources/kontekst.txt";
	protected static WorkSpaceSplitPane workSpace;
	
	public ArrayList<String> getListaKontekst() {
		return listaKontekst;
	}
	public void setListaKontekst(ArrayList<String> listaKontekst) {
		this.listaKontekst = listaKontekst;
	}
	public String getAdresaKonteksta() {
		return adresaKonteksta;
	}
	public void setAdresaKonteksta(String adresaKonteksta) {
		this.adresaKonteksta = adresaKonteksta;
	}
	
	public Kontekst() {
		super();
	}
	
	public Kontekst(ArrayList<String> listaKontekst, String adresaKonteksta) {
		super();
		this.listaKontekst = listaKontekst;
		this.adresaKonteksta = adresaKonteksta;
	}
	
	public static Boolean proveriDaLiImaSacuvaniKontekst() {
		File f = new File(adresaKonteksta);
		if(f.length() != 0) { 
		    return true;
		}
		return false;
		
	}

	public static Boolean ucitajSacuvaniKontekst() {
		//nadji fajl
		//iscitaj adrese
		ArrayList<String> adreseDatoteka = CSVHelper.ucitajCSV(adresaKonteksta);
		//kreiraj tabove
		for (String adresa : adreseDatoteka) {
			try {
				if (adresa.contains(".")) {
					String filePath = FileHelper.createPathByFileName(adresa);
					workSpace.addNewTab(adresa, filePath);
				}
				else {
					workSpace.addTableFromDB2(adresa);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return false;
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	public static Boolean sacuvajTrenutniKontekst() {
		//povuci otvorene tabove
		ArrayList<String> listaOtvorenihTabova = WorkSpaceSplitPane.dobaviOtvoreneTabove();
		//sacuvaj u kontekst fajl
		CSVHelper.upisiUCSV(adresaKonteksta, listaOtvorenihTabova);
		return true;
	}
	
	
}
