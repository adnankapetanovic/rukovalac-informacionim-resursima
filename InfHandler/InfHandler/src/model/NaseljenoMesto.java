package model;

public class NaseljenoMesto {
	private String drzava;
	private int oznaka;
	private String naziv;
	private String pttOznaka;
	
	public String getDrzava() {
		return drzava;
	}
	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}
	public int getOznaka() {
		return oznaka;
	}
	public void setOznaka(int oznaka) {
		this.oznaka = oznaka;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getPttOznaka() {
		return pttOznaka;
	}
	public void setPttOznaka(String pttOznaka) {
		this.pttOznaka = pttOznaka;
	}
	
	public NaseljenoMesto() {
		super();
	}
	
	public NaseljenoMesto(String drzava, int oznaka, String naziv, String pttOznaka) {
		super();
		this.drzava = drzava;
		this.oznaka = oznaka;
		this.naziv = naziv;
		this.pttOznaka = pttOznaka;
	}
}
