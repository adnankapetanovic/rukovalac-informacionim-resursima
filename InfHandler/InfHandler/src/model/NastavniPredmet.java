package model;

public class NastavniPredmet {
	private VisokoskolskaUstanova ustanova;
	private int predmet;
	private String naziv;
	private int ESPB;
	private boolean izbornaPozicija;
	private double biraSe;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public int getPredmet() {
		return predmet;
	}
	public void setPredmet(int predmet) {
		this.predmet = predmet;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getESPB() {
		return ESPB;
	}
	public void setESPB(int eSPB) {
		ESPB = eSPB;
	}
	public boolean isIzbornaPozicija() {
		return izbornaPozicija;
	}
	public void setIzbornaPozicija(boolean izbornaPozicija) {
		this.izbornaPozicija = izbornaPozicija;
	}
	public double getBiraSe() {
		return biraSe;
	}
	public void setBiraSe(double biraSe) {
		this.biraSe = biraSe;
	}
	
	public NastavniPredmet() {
		super();
	}
	
	public NastavniPredmet(VisokoskolskaUstanova ustanova, int predmet, String naziv, int eSPB, boolean izbornaPozicija,
			double biraSe) {
		super();
		this.ustanova = ustanova;
		this.predmet = predmet;
		this.naziv = naziv;
		ESPB = eSPB;
		this.izbornaPozicija = izbornaPozicija;
		this.biraSe = biraSe;
	}
	
	
}
