package model;

public class Nastavnik {
	private VisokoskolskaUstanova ustanova;
	private int ident;
	private String prezime;
	private String imeRoditelja;
	private String ime;
	private String JMBG;
	private char pol;
	private String telefon;
	private String adresaStanovanja;
	private String uDrzavi;
	private int uMestu;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public int getIdent() {
		return ident;
	}
	public void setIdent(int ident) {
		this.ident = ident;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getImeRoditelja() {
		return imeRoditelja;
	}
	public void setImeRoditelja(String imeRoditelja) {
		this.imeRoditelja = imeRoditelja;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getJMBG() {
		return JMBG;
	}
	public void setJMBG(String jMBG) {
		JMBG = jMBG;
	}
	public char getPol() {
		return pol;
	}
	public void setPol(char pol) {
		this.pol = pol;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getAdresaStanovanja() {
		return adresaStanovanja;
	}
	public void setAdresaStanovanja(String adresaStanovanja) {
		this.adresaStanovanja = adresaStanovanja;
	}
	public String getuDrzavi() {
		return uDrzavi;
	}
	public void setuDrzavi(String uDrzavi) {
		this.uDrzavi = uDrzavi;
	}
	public int getuMestu() {
		return uMestu;
	}
	public void setuMestu(int uMestu) {
		this.uMestu = uMestu;
	}
	
	public Nastavnik() {
		super();
	}
	
	public Nastavnik(VisokoskolskaUstanova ustanova, int ident, String prezime, String imeRoditelja, String ime,
			String jMBG, char pol, String telefon, String adresaStanovanja, String uDrzavi, int uMestu) {
		super();
		this.ustanova = ustanova;
		this.ident = ident;
		this.prezime = prezime;
		this.imeRoditelja = imeRoditelja;
		this.ime = ime;
		JMBG = jMBG;
		this.pol = pol;
		this.telefon = telefon;
		this.adresaStanovanja = adresaStanovanja;
		this.uDrzavi = uDrzavi;
		this.uMestu = uMestu;
	}
}
