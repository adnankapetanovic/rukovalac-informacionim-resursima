package model;

public class NivoStudija {
	private double oznaka;
	private String naziv;
	
	public double getOznaka() {
		return oznaka;
	}
	public void setOznaka(double oznaka) {
		this.oznaka = oznaka;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	public NivoStudija() {
		super();
	}
	
	public NivoStudija(double oznaka, String naziv) {
		super();
		this.oznaka = oznaka;
		this.naziv = naziv;
	}
}
