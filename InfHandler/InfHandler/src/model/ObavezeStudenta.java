package model;

import java.time.LocalDate;

public class ObavezeStudenta {
	private VisokoskolskaUstanova ustanova;
	private String brojIndeksa;
	private int brojObaveze;
	private int slusaKod;
	private String predmet;
	private LocalDate odKada;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public String getBrojIndeksa() {
		return brojIndeksa;
	}
	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}
	public int getBrojObaveze() {
		return brojObaveze;
	}
	public void setBrojObaveze(int brojObaveze) {
		this.brojObaveze = brojObaveze;
	}
	public int getSlusaKod() {
		return slusaKod;
	}
	public void setSlusaKod(int slusaKod) {
		this.slusaKod = slusaKod;
	}
	public String getPredmet() {
		return predmet;
	}
	public void setPredmet(String predmet) {
		this.predmet = predmet;
	}
	public LocalDate getOdKada() {
		return odKada;
	}
	public void setOdKada(LocalDate odKada) {
		this.odKada = odKada;
	}

	public ObavezeStudenta() {
		super();
	}
	
	public ObavezeStudenta(VisokoskolskaUstanova ustanova, String brojIndeksa, int brojObaveze, int slusaKod,
			String predmet, LocalDate odKada) {
		super();
		this.ustanova = ustanova;
		this.brojIndeksa = brojIndeksa;
		this.brojObaveze = brojObaveze;
		this.slusaKod = slusaKod;
		this.predmet = predmet;
		this.odKada = odKada;
	}
}
