package model;

public class PlanStudijskeGrupe {
	private VisokoskolskaUstanova ustanova;
	private String program;
	private double blok;
	private double pozicija;
	private String nastavniPredmet;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public double getBlok() {
		return blok;
	}
	public void setBlok(double blok) {
		this.blok = blok;
	}
	public double getPozicija() {
		return pozicija;
	}
	public void setPozicija(double pozicija) {
		this.pozicija = pozicija;
	}
	public String getNastavniPredmet() {
		return nastavniPredmet;
	}
	public void setNastavniPredmet(String nastavniPredmet) {
		this.nastavniPredmet = nastavniPredmet;
	}
	
	public PlanStudijskeGrupe() {
		super();
	}
	
	public PlanStudijskeGrupe(VisokoskolskaUstanova ustanova, String program, double blok, double pozicija,
			String nastavniPredmet) {
		super();
		this.ustanova = ustanova;
		this.program = program;
		this.blok = blok;
		this.pozicija = pozicija;
		this.nastavniPredmet = nastavniPredmet;
	}
	
	
}
