package model;

import java.time.LocalDate;

public class SastavDrzave {
	private String maticnaDrzava;
	private double pozicija;
	private String clanica;
	private LocalDate odKada;
	private LocalDate doKada;
	
	public String getMaticnaDrzava() {
		return maticnaDrzava;
	}
	public void setMaticnaDrzava(String maticnaDrzava) {
		this.maticnaDrzava = maticnaDrzava;
	}
	public double getPozicija() {
		return pozicija;
	}
	public void setPozicija(double pozicija) {
		this.pozicija = pozicija;
	}
	public String getClanica() {
		return clanica;
	}
	public void setClanica(String clanica) {
		this.clanica = clanica;
	}
	public LocalDate getOdKada() {
		return odKada;
	}
	public void setOdKada(LocalDate odKada) {
		this.odKada = odKada;
	}
	public LocalDate getDoKada() {
		return doKada;
	}
	public void setDoKada(LocalDate doKada) {
		this.doKada = doKada;
	}
	
	public SastavDrzave() {
		super();
	}
	
	public SastavDrzave(String maticnaDrzava, double pozicija, String clanica, LocalDate odKada, LocalDate doKada) {
		super();
		this.maticnaDrzava = maticnaDrzava;
		this.pozicija = pozicija;
		this.clanica = clanica;
		this.odKada = odKada;
		this.doKada = doKada;
	}
}
