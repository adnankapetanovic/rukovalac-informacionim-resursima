package model;

public class StrukturaUstanove {
	private String maticna;
	private String uSastavu;
	
	public String getMaticna() {
		return maticna;
	}
	public void setMaticna(String maticna) {
		this.maticna = maticna;
	}
	public String getuSastavu() {
		return uSastavu;
	}
	public void setuSastavu(String uSastavu) {
		this.uSastavu = uSastavu;
	}
	
	public StrukturaUstanove() {
		super();
	}
	
	public StrukturaUstanove(String maticna, String uSastavu) {
		super();
		this.maticna = maticna;
		this.uSastavu = uSastavu;
	}
}
