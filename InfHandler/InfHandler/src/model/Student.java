package model;

import java.time.LocalDate;

public class Student {
	private VisokoskolskaUstanova ustanova;
	private String brojIndeksa;
	private String prezime;
	private String imeRoditelja;
	private String ime;
	private char pol;
	private String telefon;
	private String JMBG;
	private LocalDate datumRodjenja;
	private String adresaStanovanja;
	private String uDrzavi;
	private int uMestu;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public String getBrojIndeksa() {
		return brojIndeksa;
	}
	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getImeRoditelja() {
		return imeRoditelja;
	}
	public void setImeRoditelja(String imeRoditelja) {
		this.imeRoditelja = imeRoditelja;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public char getPol() {
		return pol;
	}
	public void setPol(char pol) {
		this.pol = pol;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getJMBG() {
		return JMBG;
	}
	public void setJMBG(String jMBG) {
		JMBG = jMBG;
	}
	public LocalDate getDatumRodjenja() {
		return datumRodjenja;
	}
	public void setDatumRodjenja(LocalDate datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}
	public String getAdresaStanovanja() {
		return adresaStanovanja;
	}
	public void setAdresaStanovanja(String adresaStanovanja) {
		this.adresaStanovanja = adresaStanovanja;
	}
	public String getuDrzavi() {
		return uDrzavi;
	}
	public void setuDrzavi(String uDrzavi) {
		this.uDrzavi = uDrzavi;
	}
	public int getuMestu() {
		return uMestu;
	}
	public void setuMestu(int uMestu) {
		this.uMestu = uMestu;
	}
	
	public Student() {
		super();
	}
	
	public Student(VisokoskolskaUstanova ustanova, String brojIndeksa, String prezime, String imeRoditelja, String ime,
			char pol, String telefon, String jMBG, LocalDate datumRodjenja, String adresaStanovanja, String uDrzavi,
			int uMestu) {
		super();
		this.ustanova = ustanova;
		this.brojIndeksa = brojIndeksa;
		this.prezime = prezime;
		this.imeRoditelja = imeRoditelja;
		this.ime = ime;
		this.pol = pol;
		this.telefon = telefon;
		JMBG = jMBG;
		this.datumRodjenja = datumRodjenja;
		this.adresaStanovanja = adresaStanovanja;
		this.uDrzavi = uDrzavi;
		this.uMestu = uMestu;
	}
}
