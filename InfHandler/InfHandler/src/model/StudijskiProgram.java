package model;

public class StudijskiProgram {
	private VisokoskolskaUstanova ustanova;
	private String program;
	private String naziv;
	private double nivoStudija;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getNivoStudija() {
		return nivoStudija;
	}
	public void setNivoStudija(double nivoStudija) {
		this.nivoStudija = nivoStudija;
	}
	
	public StudijskiProgram() {
		super();
	}
	
	public StudijskiProgram(VisokoskolskaUstanova ustanova, String program, String naziv, double nivoStudija) {
		super();
		this.ustanova = ustanova;
		this.program = program;
		this.naziv = naziv;
		this.nivoStudija = nivoStudija;
	}
}
