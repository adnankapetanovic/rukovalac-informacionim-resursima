package model;

import java.time.LocalDate;

public class TokStudija {
	private VisokoskolskaUstanova ustanova;
	private int brojIndeksa;
	private int brojUpisa;
	private String program;
	private String skolskaGodina;
	private int godinaStudija;
	private int blok;
	private LocalDate datumUpisa;
	private LocalDate datumOvere;
	private int ESPBPocetni;
	private int ESPBKrajnji;
	
	public VisokoskolskaUstanova getUstanova() {
		return ustanova;
	}
	public void setUstanova(VisokoskolskaUstanova ustanova) {
		this.ustanova = ustanova;
	}
	public int getBrojIndeksa() {
		return brojIndeksa;
	}
	public void setBrojIndeksa(int brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}
	public int getBrojUpisa() {
		return brojUpisa;
	}
	public void setBrojUpisa(int brojUpisa) {
		this.brojUpisa = brojUpisa;
	}
	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public String getSkolskaGodina() {
		return skolskaGodina;
	}
	public void setSkolskaGodina(String skolskaGodina) {
		this.skolskaGodina = skolskaGodina;
	}
	public int getGodinaStudija() {
		return godinaStudija;
	}
	public void setGodinaStudija(int godinaStudija) {
		this.godinaStudija = godinaStudija;
	}
	public int getBlok() {
		return blok;
	}
	public void setBlok(int blok) {
		this.blok = blok;
	}
	public LocalDate getDatumUpisa() {
		return datumUpisa;
	}
	public void setDatumUpisa(LocalDate datumUpisa) {
		this.datumUpisa = datumUpisa;
	}
	public LocalDate getDatumOvere() {
		return datumOvere;
	}
	public void setDatumOvere(LocalDate datumOvere) {
		this.datumOvere = datumOvere;
	}
	public int getESPBPocetni() {
		return ESPBPocetni;
	}
	public void setESPBPocetni(int eSPBPocetni) {
		ESPBPocetni = eSPBPocetni;
	}
	public int getESPBKrajnji() {
		return ESPBKrajnji;
	}
	public void setESPBKrajnji(int eSPBKrajnji) {
		ESPBKrajnji = eSPBKrajnji;
	}
	
	public TokStudija() {
		super();
	}
	
	public TokStudija(VisokoskolskaUstanova ustanova, int brojIndeksa, int brojUpisa, String program,
			String skolskaGodina, int godinaStudija, int blok, LocalDate datumUpisa, LocalDate datumOvere,
			int eSPBPocetni, int eSPBKrajnji) {
		super();
		this.ustanova = ustanova;
		this.brojIndeksa = brojIndeksa;
		this.brojUpisa = brojUpisa;
		this.program = program;
		this.skolskaGodina = skolskaGodina;
		this.godinaStudija = godinaStudija;
		this.blok = blok;
		this.datumUpisa = datumUpisa;
		this.datumOvere = datumOvere;
		ESPBPocetni = eSPBPocetni;
		ESPBKrajnji = eSPBKrajnji;
	}
}
