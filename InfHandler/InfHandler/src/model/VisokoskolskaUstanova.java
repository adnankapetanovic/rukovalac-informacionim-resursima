package model;

public class VisokoskolskaUstanova {
	private String oznaka;
	private String naziv;
	private String adresa;
	private String sedisteDrzava;
	private int sedisteMesto;
	
	public String getOznaka() {
		return oznaka;
	}
	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getSedisteDrzava() {
		return sedisteDrzava;
	}
	public void setSedisteDrzava(String sedisteDrzava) {
		this.sedisteDrzava = sedisteDrzava;
	}
	public int getSedisteMesto() {
		return sedisteMesto;
	}
	public void setSedisteMesto(int sedisteMesto) {
		this.sedisteMesto = sedisteMesto;
	}
	
	public VisokoskolskaUstanova() {
		super();
	}
	
	public VisokoskolskaUstanova(String oznaka, String naziv, String adresa, String sedisteDrzava, int sedisteMesto) {
		super();
		this.oznaka = oznaka;
		this.naziv = naziv;
		this.adresa = adresa;
		this.sedisteDrzava = sedisteDrzava;
		this.sedisteMesto = sedisteMesto;
	}
}
