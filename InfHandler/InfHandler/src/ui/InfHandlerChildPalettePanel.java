package ui;

import javax.swing.JPanel;

public class InfHandlerChildPalettePanel extends JPanel{
	public void init() {
		InfHandlerPaletteBar paletteBar = new InfHandlerPaletteBar();
		this.add(paletteBar);
        paletteBar.init();
	}

}
