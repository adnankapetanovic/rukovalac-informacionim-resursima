package ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.border.Border;

import helper.DBHelper;
import helper.FileHelper;

public class InfHandlerDataSplitPane extends JSplitPane {
	//public static SystemTreeView dataFileTree = new SystemTreeView(new File("data/Datoteke"));
	public static SystemTreeView dataFileTree = new SystemTreeView(new File("data/Datoteke"));
	public static String connectionString = null;
	public static SystemTreeView dataBaseTree = new SystemTreeView(connectionString);
	JList<String> displayList;
	JPanel dataPanel;
	
	public InfHandlerDataSplitPane() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InfHandlerDataSplitPane(int newOrientation, boolean newContinuousLayout, Component newLeftComponent,
			Component newRightComponent) {
		super(newOrientation, newContinuousLayout, newLeftComponent, newRightComponent);
		// TODO Auto-generated constructor stub
	}

	public InfHandlerDataSplitPane(int newOrientation, boolean newContinuousLayout) {
		super(newOrientation, newContinuousLayout);
		// TODO Auto-generated constructor stub
	}

	public InfHandlerDataSplitPane(int newOrientation, Component newLeftComponent, Component newRightComponent) {
		super(newOrientation, newLeftComponent, newRightComponent);
		// TODO Auto-generated constructor stub
	}

	public InfHandlerDataSplitPane(int newOrientation) {
		super(newOrientation);
		// TODO Auto-generated constructor stub
	}

	protected void init() throws SQLException {
		Border blacklineData = BorderFactory.createTitledBorder("File Explorer");
		Border blacklineDataBase = BorderFactory.createTitledBorder("Database");
		dataPanel = new JPanel();
		JPanel dbPanel = new JPanel();

		JPanel dataSourceTablePanel = new JPanel();
		dataSourceTablePanel.setLayout(new FlowLayout());

		// data file panel
		dataPanel.add(dataFileTree);
		dataPanel.setBorder(blacklineData);

		// database panel
		DBHelper dbHelper = new DBHelper();
		displayList = new JList<>(dbHelper.getDBTablesList().toArray(new String[0]));
		JScrollPane scrollPaneDB = new JScrollPane(displayList);
		
		dbPanel.add(scrollPaneDB);
		dbPanel.setBorder(blacklineDataBase);
		
		JScrollPane scrollPaneData = new JScrollPane(dataPanel);
		this.add(scrollPaneData);
		this.add(dbPanel);
}

	public void drawDataTree(String string, MouseListener[] mList) {
		System.out.println(string);
		this.dataPanel.remove(dataFileTree);
		SystemTreeView dataFileTree = new SystemTreeView(new File("data/Datoteke/"+string));
		for (MouseListener mouseListener : mList) {
			dataFileTree.addMouseListener(mouseListener);
		}
		this.dataPanel.add(dataFileTree);
	}
}
