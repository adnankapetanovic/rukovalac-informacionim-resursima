package ui;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import model.Kontekst;

public class InfHandlerKonteksPrompt {

	public static void propmptZaOtvaranjeKontekstaProsleSesije() {
		
		String naslov = "Otvaranje Konteksta Prethodne Sesjije";
		String porukaUspeh = "Kontekst uspesno otvoren.";
		String porukaNeuspeh = "Kontekst neuspesno otvoren";
		
		int option = JOptionPane.showConfirmDialog(null,
				"Postoji sacuvani kontekst od prosle sesije. Da li zelite da nastavite sa radom?",
				"Nastavi rad",
				JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
		if(option==0){
			
			Boolean success = Kontekst.ucitajSacuvaniKontekst();
	    	
	    	if (success) {
	    		generisiPorukuNakonIzvrsenjaAkcije(naslov, porukaUspeh);
			}
	    	else {
	    		generisiPorukuNakonIzvrsenjaAkcije(naslov, porukaNeuspeh);
	    	}
		}
}
	public static void promptZaCuvanjeKontekstaPreZatvaranjaAplikacije() {
		
		String naslov = "Cuvanje Konteksta Pre Zatvaranja Aplikacije";
		String porukaUspeh = "Kontekst uspesno sacuvan.";
		String porukaNeuspeh = "Kontekst neuspesno sacuvan";
		
		int option = JOptionPane.showConfirmDialog(null,
				"Da li zelite da sacuvate kontekst pre zatvaranja aplikacije?",
				"Cuvanje Konteksta",
				JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
		if(option==0){
			
			Boolean success = Kontekst.sacuvajTrenutniKontekst();
	    	
	    	if (success) {
	    		generisiPorukuNakonIzvrsenjaAkcije(naslov, porukaUspeh);
			}
	    	else {
	    		generisiPorukuNakonIzvrsenjaAkcije(naslov, porukaNeuspeh);
	    	}
		}
		else {
			PrintWriter pw = null;
			try {
				pw = new PrintWriter("resources/kontekst.txt");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pw.close();
		}
	}
	
	private static void generisiPorukuNakonIzvrsenjaAkcije(String naslov, String poruka) {
		JDialog zMessageDialog = new JDialog((java.awt.Frame) null, true);
	    zMessageDialog.setTitle(naslov);
	    zMessageDialog.setLayout(new BorderLayout());
	    JTextArea zTextArea = new JTextArea(poruka);
	    zTextArea.setEditable(false);
	    zTextArea.setColumns(40);
	    zTextArea.setRows(10);
	    zTextArea.setBackground(null);
	    JScrollPane zScrollPane = new JScrollPane(zTextArea);
	    zMessageDialog.add(zScrollPane, BorderLayout.CENTER);
	    zMessageDialog.revalidate();
	    zMessageDialog.pack();
	    zMessageDialog.setVisible(true);
	}
}
