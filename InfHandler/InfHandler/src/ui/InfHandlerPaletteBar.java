package ui;

import javax.swing.JButton;
import javax.swing.JToolBar;

public class InfHandlerPaletteBar extends JToolBar {
	public void init() {
        JButton openRecordButton = new JButton("Open record");
        JButton saveRecordButton = new JButton("Update Record");
        JButton deleteRecordButton = new JButton("Delete Record");
        JButton findRecordButton = new JButton("Find Record");
        
        this.add(openRecordButton);
        this.add(saveRecordButton);
        this.add(deleteRecordButton);
        this.add(findRecordButton);
        this.setFloatable(false);
	}
}

