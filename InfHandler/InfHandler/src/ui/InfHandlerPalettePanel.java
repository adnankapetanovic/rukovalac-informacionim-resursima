package ui;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;

public class InfHandlerPalettePanel extends JPanel {
	

	    protected void init() {
	    	JPanel paletteInfoPanel = new JPanel();
	        JLabel fileSizeLabel = new JLabel("File size: ");
	        JTextField fileSizeText = new JTextField(10);
	        fileSizeText.setEditable(false);
	        JLabel recordSizeLabel = new JLabel("Record size(B): ");
	        JTextField recordSizeText = new JTextField(10);
	        recordSizeText.setEditable(false);
	        JLabel recordNumLabel = new JLabel("Record num: ");
	        JTextField recordNumText = new JTextField(10);
	        recordNumText.setEditable(false);
	        paletteInfoPanel.add(fileSizeLabel);
	        paletteInfoPanel.add(fileSizeText);
	        paletteInfoPanel.add(recordSizeLabel);
	        paletteInfoPanel.add(recordSizeText);
	        paletteInfoPanel.add(recordNumLabel);
	        paletteInfoPanel.add(recordNumText);
	        paletteInfoPanel.setBackground(Color.blue);
	        
	        JToolBar PalettePanel = new JToolBar();
	        JButton openRecordButton = new JButton("Open record");
	        JButton editRecordButton = new JButton("Edit Record");
	        JButton saveRecordButton = new JButton("Update Record");
	        JButton deleteRecordButton = new JButton("Delete Record");
	        JButton findRecordButton = new JButton("Find Record");
	        
	        paletteInfoPanel.add(openRecordButton);
	        paletteInfoPanel.add(editRecordButton);
	        paletteInfoPanel.add(saveRecordButton);
	        paletteInfoPanel.add(deleteRecordButton);
	        paletteInfoPanel.add(findRecordButton);
	        
	        this.add(paletteInfoPanel);
	        this.add(PalettePanel);
	    }  
	        
	        
	
}
