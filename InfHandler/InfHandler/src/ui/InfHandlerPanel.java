package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.io.File;
import java.io.FileFilter;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;

public class InfHandlerPanel extends JPanel{

	protected void init() {
		
		JSplitPane dataSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSplitPane dataSourceTableSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenu databaseMenu = new JMenu("Database");
		JMenu helpMenu = new JMenu("Help");
		
		JToolBar toolbar = new JToolBar("Toolbar");
		JButton newFileButton = new JButton("New File");
		JButton saveFileButton = new JButton("Save File");
		JButton closeFileButton = new JButton("Close File");
		JButton closeAllButton = new JButton("Close All");
		
		JToolBar dataSourceToolbar = new JToolBar("Data Source Display");
		
		
		File file = new File("data");
		File[] files = file.listFiles(new FileFilter() {
		    @Override
		    public boolean accept(File f) {
		        return f.isDirectory();
		    }
		});
		System.out.println("Folders count: " + files.length);
		
	    //File[] f = File.listRoots();
	    for (int i = 0; i < files.length; i++)
	    {
	    	dataSourceToolbar.add(new JButton(files[i].getName()));
	    }

		toolbar.add(newFileButton);
		toolbar.add(saveFileButton);
		toolbar.add(closeFileButton);
		toolbar.add(closeAllButton);
		
		menuBar.add(fileMenu);
		menuBar.add(databaseMenu);
		menuBar.add(helpMenu);

		toolbar.setFloatable(false);
		dataSourceToolbar.setFloatable(false);
		
		JPanel toolbarPanel = new JPanel();
		JPanel menuPanel = new JPanel();
		JPanel dataSourcePanel = new JPanel();
		JPanel toolMenuPanel = new JPanel();
		
		toolMenuPanel.setLayout(new GridLayout(3, 1));
		toolMenuPanel.setSize(800, 600);
		
		toolMenuPanel.add(menuBar);
		toolMenuPanel.add(toolbar);
		toolMenuPanel.add(dataSourceToolbar);

		Border blacklineData = BorderFactory.createTitledBorder("File Explorer");
		Border blacklineDataBase = BorderFactory.createTitledBorder("Database");
	    JPanel panel = new JPanel();
	    LayoutManager layout = new FlowLayout();  
	           

	    JPanel dataPanel = new JPanel();
	    JPanel dbPanel = new JPanel();
	    InfHandlerTablePanel tablePanel = new InfHandlerTablePanel();
	    JPanel dataSourceTablePanel = new JPanel();
	    dataSourceTablePanel.setLayout(new FlowLayout());
	    
	    SystemTreeView dataTree = new SystemTreeView(new File("data"));

	    dataPanel.add(dataTree);
	    dataPanel.setBorder(blacklineData);
	    //dataPanel.setLayout(layout);
	    

	    //database panel
	    dbPanel.setBorder(blacklineDataBase);
	    
	    dataSplitPane.add(dataPanel);
	    dataSplitPane.add(dbPanel);
	    
	    tablePanel.init();
	    JTable table = new JTable();
	    JScrollPane scrollPane = new JScrollPane(table);
	    //tabbedPane.add(scrollPane);
	    
	    dataSourceTableSplitPane.add(dataSplitPane);
	    dataSourceTableSplitPane.add(tablePanel);
	    
	    this.setLayout(new BorderLayout());
	    this.add(toolMenuPanel, BorderLayout.PAGE_START);
	    
	    this.add(dataSourceTableSplitPane, BorderLayout.CENTER);
	    this.setLayout(new BorderLayout());
	}
}
