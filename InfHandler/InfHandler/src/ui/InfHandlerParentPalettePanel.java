package ui;

import javax.swing.JButton;
import javax.swing.JPanel;

public class InfHandlerParentPalettePanel extends JPanel {
	
static JButton closeTabButton = new JButton("Close Tab");
    static JButton closeAllTabsButton = new JButton("Close All Tabs");
    
	public void init() {
		InfHandlerPaletteBar paletteBar = new InfHandlerPaletteBar();
        this.add(paletteBar);
        paletteBar.init();
        paletteBar.addSeparator();
        paletteBar.add(closeTabButton);
        paletteBar.add(closeAllTabsButton);
        
	}
    public void addCloseTabListener(InfHandlerTablePanel infHandlerTablePanel) {
        this.closeTabButton.addActionListener(e -> {
			for (String tab : infHandlerTablePanel.openTabs) {
				infHandlerTablePanel.tabbedChildPane.removeAll();
				infHandlerTablePanel.tabbedPane.remove(infHandlerTablePanel.tabbedPane.getSelectedIndex());
			}
			infHandlerTablePanel.openTabs.clear();
		});
	}
        
    public void addCloseTabsListener(InfHandlerTablePanel infHandlerTablePanel) {
        this.closeAllTabsButton.addActionListener(e -> {
			int j = infHandlerTablePanel.tabbedPane.getSelectedIndex();
			infHandlerTablePanel.tabbedChildPane.removeAll();
			infHandlerTablePanel.tabbedPane.closeTab();
			int i = infHandlerTablePanel.openTabs.size();
			infHandlerTablePanel.openTabs.remove(infHandlerTablePanel.tabbedPane.getTitleAt(j));
		});
     }
	}
