package ui;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class InfHandlerPopUp extends JPopupMenu{
	
	String path = "data/01_TeritorijalnaOrganizacija/Drzava.txt";
    
    public InfHandlerPopUp() {
    	JMenuItem openMenuItem = new JMenuItem("Open");
    	JMenuItem updateMenuItem = new JMenuItem("Update");
        add(openMenuItem);
        
        openMenuItem.addActionListener(e -> {
        	//InfHandlerTable tabela = InfHandlerTable.populateTable(path);
        	TabbedInformationDisplay tabbDisplay = new TabbedInformationDisplay();
        	//tabbDisplay.addTab("New", tabela);
        });
		
        //na klik polja treba da se pokrene init za novi tab i da se za dati fajl otvori tabela
        
//		openMenuItem.addActionListener(e -> { FileTable.showTable(); });
//		  
//		  
//		  prodavciMenuItem.addActionListener(e -> { ProdavacDialog prodavacDialog =
//		  null; try { prodavacDialog = new ProdavacDialog(); } catch (IOException e1) {
//		  // TODO Auto-generated catch block e1.printStackTrace(); }
//		  prodavacDialog.setVisible(true); });
		 
    }

	private JTable kreirajTabelu() {
        // Data to be displayed in the JTable
        String[][] data = {
            { "Kundan Kumar Jha", "4031", "CSE" },
            { "Anand Jha", "6014", "IT" }
        };
 
        // Column Names
        String[] columnNames = { "Name", "Roll Number", "Department" };
 
        // Initializing the JTable
        JTable j = new JTable(data, columnNames);
        j.setBounds(30, 40, 200, 300);
		return j;
	}
}
