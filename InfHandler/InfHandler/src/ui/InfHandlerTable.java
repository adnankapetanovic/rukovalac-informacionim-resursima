package ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.Popup;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import delegate.PopClickListener;
import helper.FileHelper;

public class InfHandlerTable extends JTable{

	private static final long serialVersionUID = 1L;
//	
//    JPopupMenu popupMenu = new JPopupMenu();
//    JMenuItem deleteItem = new JMenuItem("Delete");
    
    
	public InfHandlerTable() {
		super();
	    JPopupMenu popupMenu = new JPopupMenu();
	    JMenuItem deleteItem = new JMenuItem("Delete");
	    popupMenu.add(deleteItem);
		this.setComponentPopupMenu(popupMenu);
		this.addMouseListener(new PopClickListener(this));

	}

	public InfHandlerTable(int numRows, int numColumns) {
		super(numRows, numColumns);
		// TODO Auto-generated constructor stub
	}

	public InfHandlerTable(Object[][] rowData, Object[] columnNames, String path) {
		super(rowData, columnNames);
	    JPopupMenu popupMenu = new JPopupMenu();
	    JMenuItem deleteItem = new JMenuItem("Delete");
	    deleteItem.addActionListener(e -> {
	    	JTable source = (JTable)e.getSource();
	    	int option = JOptionPane.showConfirmDialog(null,
	    			"Are you sure you want to delete this item?",
	    			"Delete row",
	    			JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
	    	if(option==0){
	    		
		    	int row = source.getSelectedRow();
	    		Boolean success = InfHandlerTable.removeRow(row, path);
		    	source.remove(row);
		    	
		    	if (success) {
		    		JDialog zMessageDialog = new JDialog((java.awt.Frame) null, true);
		    	    zMessageDialog.setTitle("Delete outcome");
		    	    zMessageDialog.setLayout(new BorderLayout());
		    	    JTextArea zTextArea = new JTextArea("Deletion successful!");
		    	    zTextArea.setEditable(false);
		    	    zTextArea.setColumns(40);
		    	    zTextArea.setRows(10);
		    	    zTextArea.setBackground(null);
		    	    JScrollPane zScrollPane = new JScrollPane(zTextArea);
		    	    zMessageDialog.add(zScrollPane, BorderLayout.CENTER);
		    	    zMessageDialog.revalidate();
		    	    zMessageDialog.pack();
		    	    zMessageDialog.setVisible(true);
				}
		    	else {
		    		JDialog zMessageDialog = new JDialog((java.awt.Frame) null, true);
		    	    zMessageDialog.setTitle("Delete outcome");
		    	    zMessageDialog.setLayout(new BorderLayout());
		    	    JTextArea zTextArea = new JTextArea("Deletion not successful!");
		    	    zTextArea.setEditable(false);
		    	    zTextArea.setColumns(40);
		    	    zTextArea.setRows(10);
		    	    zTextArea.setBackground(null);
		    	    JScrollPane zScrollPane = new JScrollPane(zTextArea);
		    	    zMessageDialog.add(zScrollPane, BorderLayout.CENTER);
		    	    zMessageDialog.revalidate();
		    	    zMessageDialog.pack();
		    	    zMessageDialog.setVisible(true);
		    	}
	    	}
	    });
	    popupMenu.add(deleteItem);
		this.setComponentPopupMenu(popupMenu);
		this.addMouseListener(new PopClickListener(this));
	}

	private static Boolean removeRow(int row, String path) {
		Boolean success = false;
		//nadji data
		ArrayList<String> loadedCSV = loadCSV(path);
		//obrisi red
		loadedCSV.remove(row);
		//prepisi data
		success = saveCSV(loadedCSV, path);
		return success;
	}

	private static Boolean saveCSV(ArrayList<String> loadedCSV, String path) {
		Boolean success = false;
		try {
			File file = new File(path);
			FileWriter writer = new FileWriter(path);  
		    BufferedWriter buffer = new BufferedWriter(writer);  
		    for (String unos : loadedCSV) {
				buffer.write(unos);
			}
		    buffer.flush();
		    buffer.close();  
		    success = true; 
			
			FileOutputStream fos = new  FileOutputStream(file, true);
			//fos.write(data.objekatUCSVFormat().getBytes());
			fos.write("\n".getBytes());
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fajl nije nadjen!");
		} catch (IOException e) {
			System.out.println("Doslo je do problema prilikom upisivanja podataka u datoteku!");
		}
		return success;
	}
		

	public InfHandlerTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
		super(dm, cm, sm);
		// TODO Auto-generated constructor stub
	}

	public InfHandlerTable(TableModel dm, TableColumnModel cm) {
		super(dm, cm);
		// TODO Auto-generated constructor stub
	}

	public InfHandlerTable(TableModel dm) {
		super(dm);
		// TODO Auto-generated constructor stub
	}

	public InfHandlerTable(Vector<? extends Vector> rowData, Vector<?> columnNames) {
		super(rowData, columnNames);
		// TODO Auto-generated constructor stub
	}
	
	public InfHandlerTable(JTable jTable) {
		// TODO Auto-generated constructor stub
	}

	public void addNewRecord(String path, String data) {
		try {
			FileWriter writer = new FileWriter(path);  
		    BufferedWriter buffer = new BufferedWriter(writer);  
		    buffer.append(data);
		    buffer.flush();
		    buffer.close();  
		    System.out.println("Novi Zapis je uspesno dodat.");
		} catch (FileNotFoundException e) {
			System.out.println("Fajl nije nadjen!");
		} catch (IOException e) {
			System.out.println("Doslo je do problema prilikom upisivanja podataka u datoteku!");
		}
	}
	
	public static void saveCSV(String path, String data, int row) {
		//Ucitamo CSV u listu kako bi umetnuli izmenjen entitet
		ArrayList<String> backupList = loadCSV(path);
		//lociramo izmenjeni entitet i prepisemo ga
		try {
			File file = new File(path);
			FileWriter writer = new FileWriter(path);  
		    BufferedWriter buffer = new BufferedWriter(writer);  
		    buffer.write(data);
		    buffer.flush();
		    buffer.close();  
		    System.out.println("Success");  
			
			FileOutputStream fos = new  FileOutputStream(file, true);
			//fos.write(data.objekatUCSVFormat().getBytes());
			fos.write("\n".getBytes());
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fajl nije nadjen!");
		} catch (IOException e) {
			System.out.println("Doslo je do problema prilikom upisivanja podataka u datoteku!");
		}
	}
	
	private static ArrayList<String> loadCSV(String path) { 
		File file = new File(path);
		BufferedReader csvReader = null;
		ArrayList<String> deserijalizovaniPodaci = new ArrayList<String>();

		try {
			FileInputStream fis = new FileInputStream(file);
			csvReader = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
			String ucitano = csvReader.readLine();
			deserijalizovaniPodaci.add(ucitano);
			while(ucitano != null) {
			    //System.out.println(line);
				ucitano = csvReader.readLine();
				if (ucitano!=null) {
					deserijalizovaniPodaci.add(ucitano);
				}
			}
			fis.close();
			csvReader.close();
			} 
			catch (FileNotFoundException e) {
				System.out.println("Fajl nije pronadjen!");
			}
			catch (IOException e) {
				System.out.println("Greska prilikom ucitavanja podataka iz datoteke!");
			}
		
		ArrayList<String> proverenaListaPodataka = new ArrayList<String>(dopunaVrednosti(deserijalizovaniPodaci));

		return proverenaListaPodataka;
	}
	
	private static ArrayList<String>dopunaVrednosti(ArrayList<String>listaPodataka){

		for (int i = 0; i < listaPodataka.size(); i++) {
			if (listaPodataka.get(i).endsWith(";")) {
				String noviPodatak = listaPodataka.get(i).concat(" ");
				listaPodataka.set(i, noviPodatak);
			}
		} 
		return listaPodataka;
	}
	
	public InfHandlerTable populateTable(String path) throws FileNotFoundException, JSONException {
		ArrayList<String> loadedCSV = loadCSV(path);
		String[] tempNIzStringova = loadedCSV.get(0).split(";");
		int dataRow = loadedCSV.size();
		int dataCol = tempNIzStringova.length;
		String data[][] = new String[dataRow][dataCol];
		String[] tempNiz;

		for (int i = 1; i < data.length; i++) {
			String nekiString2 = loadedCSV.get(i-1);
			tempNiz = nekiString2.split(";");
			data[i-1] = tempNiz;
		}
	        // Column Names
			//String[] columnNames = loadedCSV.get(0).split(";");
    		String[] tempString = path.split("/");
			String[] columnNames = getColumnNames(tempString[tempString.length-1]);
	 
	        // Initializing the JTable
			InfHandlerTable table = new InfHandlerTable(data, columnNames, path);
			return table;
	}
	
	private String[] getColumnNames(String fileName) throws FileNotFoundException, JSONException {
		//String[] columnNames;
		ArrayList<String> jsonData = new ArrayList<String>();
		String file = fileName.substring(0, fileName.length() - 4);
		File f = new File("data/Metaopisi");

		String adaptedJSONName = FileHelper.fileNametoCamelCase(file);
		File fileToSearchThrough = new File("data/Metaopisi/" + adaptedJSONName + ".json");

		if (f.exists()) {
			InputStream inputStream = new FileInputStream(fileToSearchThrough);
			Scanner s = new Scanner(inputStream).useDelimiter("\\A");
			String jsonTxt = s.hasNext() ? s.next() : "";
			JSONObject json = new JSONObject(jsonTxt);
			JSONArray content = json.getJSONArray("content");
			for (int i = 0; i < content.length(); i++) {
				JSONObject objectInArray = content.getJSONObject(i);

				String[] elementNames = JSONObject.getNames(objectInArray);
				for (String elementName : elementNames) {
					if (elementName.equals("title")) {
						String value = objectInArray.getString(elementName);
						jsonData.add(value);
					}
				}
			}
		}
		String[] columnNames = new String[jsonData.size()];
		for (int i = 0; i < columnNames.length; i++) {
			columnNames[i] = jsonData.get(i);
			
		}
		return columnNames;
	}
	
	private InfHandlerTable castToJTable(JTable jTable) {
		InfHandlerTable iHTable = new InfHandlerTable(jTable);
		return iHTable;
	}
}
