package ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.json.JSONException;

public class InfHandlerTablePanel extends JSplitPane {
	public static JScrollPane aux1ScrollPanel = new JScrollPane();
	public static JScrollPane aux2ScrollPanel = new JScrollPane();
	public static Boolean prvoOtvaranje = true;
	InfHandlerParentPalettePanel newPalete1 = new InfHandlerParentPalettePanel();
	InfHandlerChildPalettePanel newPalete2 = new InfHandlerChildPalettePanel();
	public static ArrayList<String> openTabs = new ArrayList<String>();
	JPanel auxPanel1;
	JPanel auxPanel2;
	WorkSpaceSplitPane workSpace;
	
	public InfHandlerTablePanel(int newOrientation) {
		super(newOrientation);
		// TODO Auto-generated constructor stub
	}

	TabbedInformationDisplay tabbedPane;
	TabbedDataChildDisplay tabbedChildPane;

	public InfHandlerTablePanel() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void init() {
		auxPanel1 = new JPanel();
		auxPanel2 = new JPanel();
		//this.setLayout(new BorderLayout());
		tabbedPane = new TabbedInformationDisplay();
		tabbedChildPane = new TabbedDataChildDisplay();
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedChildPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabbedPane.addChangeListener(changeListener);
		
		this.auxPanel1.setLayout(new BorderLayout());
		this.auxPanel1.add(newPalete1, BorderLayout.NORTH);
		this.auxPanel1.add(tabbedPane, BorderLayout.CENTER);
		
		this.auxPanel2.setLayout(new BorderLayout());
		this.auxPanel2.setLayout(new BorderLayout());
		this.auxPanel2.add(newPalete2, BorderLayout.NORTH);
		this.auxPanel2.add(tabbedChildPane, BorderLayout.CENTER);

//		auxPanel1.add(tabbedPane);
//		auxPanel2.add(tabbedChildPane);
		
		JScrollPane aux1ScrollPanel = new JScrollPane(auxPanel1);
		JScrollPane aux2ScrollPanel = new JScrollPane(auxPanel2);

		this.add(aux1ScrollPanel);
		this.add(aux2ScrollPanel);

	    this.newPalete1.closeAllTabsButton.addActionListener(e -> {
			for (String tab :  this.openTabs) {
				this.tabbedChildPane.removeAll();
				this.tabbedPane.remove(this.tabbedPane.getSelectedIndex());
			}
			workSpace.removeTablePanel(); 
		});

	    this.newPalete1.closeTabButton.addActionListener(e -> {
			int j = this.tabbedPane.getSelectedIndex();
			this.tabbedChildPane.removeAll();
			int i = this.tabbedPane.getTabCount();
			this.tabbedPane.closeTab();
			if (this.tabbedPane.getTabCount() == 0) {
				workSpace.removeTablePanel(); 
			}
		});
	}
	
	ChangeListener changeListener = new ChangeListener() {
	      public void stateChanged(ChangeEvent changeEvent) {
	        JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
	        int tabC = sourceTabbedPane.getTabCount();
	        //if (sourceTabbedPane.getSelectedIndex() == tabC) {
		        int index = sourceTabbedPane.getSelectedIndex();
		        //System.out.println("Tab changed to: " + sourceTabbedPane.getTitleAt(index));
		        if (index >= 0) {
			        String parent = sourceTabbedPane.getTitleAt(index);
			        //ocisti tabove
			        //otvori decu za taj tab
			        tabbedChildPane.clearTabs();
			        try {
			        	if (parent.contains(".")) {
			        		tabbedChildPane.openChildrenTabs(parent);
						}
			        	else {
			        	tabbedChildPane.openChildrenTabsDB(parent);
			        	}
					} catch (FileNotFoundException | JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			//}
	      }
	};
	


	public InfHandlerTable addNewTab(String path) throws FileNotFoundException, JSONException {
		return this.tabbedPane.addNewTable(path);
	}

	public InfHandlerTable addNewTabC(String path) throws FileNotFoundException, JSONException {
		return this.tabbedChildPane.addNewTable(path);
	}

	public void initContent() {
		if (prvoOtvaranje) {
			newPalete1.init();
			newPalete2.init();
		}
		this.prvoOtvaranje = false;
	}
}
