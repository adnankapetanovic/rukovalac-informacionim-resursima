package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.json.JSONException;

import helper.DBHelper;
import helper.FileHelper;
import model.Kontekst;

public class MainWindow extends JFrame{

	private static final long serialVersionUID = -2133534539333334278L;
	protected SpringLayout springLayout = new SpringLayout();
	JToolBar dataSourceToolbar;
	protected static WorkSpaceSplitPane workSpace;
	
	public MainWindow() throws HeadlessException {
		super();
		this.setLayout(this.springLayout);
	}

	public MainWindow(GraphicsConfiguration gc) {
		super(gc);
	}

	public MainWindow(String title, GraphicsConfiguration gc) {
		super(title, gc);
	}

	public MainWindow(String title) throws HeadlessException {
		super(title);
	}
	
	public void init() throws SQLException {
		this.setTitle("Rukovalac Informacionim Resursima");
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setSize(800, 600);
		
//		dataSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		workSpace = new WorkSpaceSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		workSpace.init();
		
		this.addWindowListener( new WindowAdapter()
		 {
		   public void windowClosing(WindowEvent e)
		    {
		      //proveri da li ima otvorenih tabova
			   if(workSpace.proveriDaLiImaOtvorenihTabova())
			   {
				   InfHandlerKonteksPrompt.promptZaCuvanjeKontekstaPreZatvaranjaAplikacije();
			   }
			   //ako ima pitaj za cuvanje
			   //ako nema - nikom nista
			   System.exit(0);
		     }
		   
		  });
		
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenu databaseMenu = new JMenu("Database");
		JMenu helpMenu = new JMenu("Help");
		
		dataSourceToolbar = new JToolBar("Data Source Display");
		
//		closeAllButton.addActionListener(e -> {
//			this.workSpace.tablePanel.tabbedPane.removeAll();
//			this.workSpace.tablePanel.tabbedChildPane.removeAll();
//		});
//		
//		closeFileButton.addActionListener(e -> {
//			this.workSpace.tablePanel.tabbedPane.closeTab();
//		});
		
		File file = new File("data/Datoteke");
		File[] files = file.listFiles(new FileFilter() {
		    @Override
		    public boolean accept(File f) {
		        return f.isDirectory();
		    }
		});
		MouseListener[] mList = workSpace.tree.getMouseListeners();
	    for (int i = 0; i < files.length; i++)
	    {
	    	JButton sourceButton = new JButton(files[i].getName());
	    	sourceButton.setName((files[i].getName()));
//	    	sourceButton.addMouseListener(new MouseAdapter() {
//	            @Override
//	            public void mousePressed(MouseEvent e) {
//	            	workSpace.dataSplitPane.drawDataTree(sourceButton.getName(), mList);
//	                //texto.setBackground(colores[index]);
//	            }           
//	        });
	    	dataSourceToolbar.add(sourceButton);
	    	
	    }
		
		menuBar.add(fileMenu);
		menuBar.add(databaseMenu);
		menuBar.add(helpMenu);


		dataSourceToolbar.setFloatable(false);
		
		JPanel toolbarPanel = new JPanel();
		JPanel menuPanel = new JPanel();
		JPanel dataSourcePanel = new JPanel();
		JPanel toolMenuPanel = new JPanel();
		
		toolMenuPanel.setLayout(new GridLayout(2, 1));
		toolMenuPanel.setSize(800, 600);
		
		toolMenuPanel.add(menuBar);
		toolMenuPanel.add(dataSourceToolbar);
	  
	 	try { 
	 		this.setIconImage(ImageIO.read(new File("resources/icons/blue-document.png")));
	 	}
	 	catch (Exception e) {
	 				// TODO: handle exception
	 	}
	    
	    JTable table = new JTable();
	    JScrollPane scrollPane = new JScrollPane(table);
	    
	    this.setLayout(new BorderLayout());
	    this.getContentPane().add(toolMenuPanel, BorderLayout.PAGE_START);
	    
	    this.getContentPane().add(workSpace, BorderLayout.CENTER);
	 	
//	 	InfHandlerPanel newPanel = new InfHandlerPanel();
//	 	newPanel.init();
//	 	
//	 	Container contentPane = this.getContentPane();
//	 	contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 0,90));
//	 	this.add(newPanel);
	    this.add(workSpace);
	    this.pack();
		this.setVisible(true);
		
		//proveri da li je ostao neki kontekst od proslog puta
		boolean postojiSacuvaniKontekst = Kontekst.proveriDaLiImaSacuvaniKontekst();
		//ako jeste ostao, pitaj da li zeli da se otvori
		if (postojiSacuvaniKontekst) {
			InfHandlerKonteksPrompt.propmptZaOtvaranjeKontekstaProsleSesije();
		}
		
	}

	}

