package ui;

import java.awt.BorderLayout;


import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Collections;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import java.util.ArrayList;

import delegate.InfHandlerDelegate;
import delegate.PopClickListener;

public class SystemTreeView extends JPanel {

	private static final long serialVersionUID = 1L;

	public static String selectedNodePath = null;
	public static JTree tree;
	
	public SystemTreeView() {
		
	}
	
	public SystemTreeView(String connectionString) {
		setLayout(new BorderLayout());
		JScrollPane scrollpane = new JScrollPane();
	    scrollpane.getViewport().add(this.createDatabaseTree(connectionString),BorderLayout.SOUTH);

	    add(BorderLayout.SOUTH, scrollpane);
	    //this.setPreferredSize(getPreferredSize());
	}
	
	public SystemTreeView(File dir) {
	    setLayout(new BorderLayout());
	    tree = new JTree(addNodes(null, dir));

	    JScrollPane scrollpane = new JScrollPane();
	    scrollpane.getViewport().add(tree,BorderLayout.PAGE_START);

	    add(BorderLayout.PAGE_START, scrollpane);
	}

	private DefaultMutableTreeNode addNodes(DefaultMutableTreeNode curTop, File dir) {
		String curPath = dir.getPath();
		DefaultMutableTreeNode curDir = new DefaultMutableTreeNode(curPath);
		if (curTop != null) { // should only be null at root
			curTop.add(curDir);
		}
		Vector<String> ol = new Vector<String>();
		String[] tmp = dir.list();
		for (int i = 0; i < tmp.length; i++)
			ol.addElement(tmp[i]);
		Collections.sort(ol, String.CASE_INSENSITIVE_ORDER);
		File f;
		Vector<String> files = new Vector<String>();
		// Make two passes, one for Dirs and one for Files. This is #1.
		for (int i = 0; i < ol.size(); i++) {
			String thisObject = (String) ol.elementAt(i);
			String newPath;
			if (curPath.equals("."))
				newPath = thisObject;
			else
				newPath = curPath + File.separator + thisObject;
			if ((f = new File(newPath)).isDirectory())
				addNodes(curDir, f);
			else
				files.addElement(thisObject);
		}
		// Pass two: for files.
		for (int fnum = 0; fnum < files.size(); fnum++)
			curDir.add(new DefaultMutableTreeNode(files.elementAt(fnum)));
		return curDir;
	}

	protected JTree createDatabaseTree(String connectionString) {
		JTree dataBaseTree = null;
		DefaultMutableTreeNode root = null;
		if (connectionString == null) {
			dataBaseTree = new JTree();
			root = new DefaultMutableTreeNode("No Database available");
			DefaultTreeModel treeModel = new DefaultTreeModel(root);
			DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) dataBaseTree.getCellRenderer();
			dataBaseTree.setEditable(true);
			ImageIcon imageIcon = new ImageIcon("resources/icons/database.png");
			Image image = imageIcon.getImage(); // transform it 
			Image newimg = image.getScaledInstance(30, 30,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
			imageIcon = new ImageIcon(newimg);
			renderer.setLeafIcon(imageIcon);
			dataBaseTree.setModel(treeModel);
		}
		return dataBaseTree;
	}
}
