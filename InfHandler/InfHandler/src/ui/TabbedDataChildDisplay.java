package ui;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import helper.DBHelper;
import helper.FileHelper;

public class TabbedDataChildDisplay extends JTabbedPane {

	private static final long serialVersionUID = 2987229421832455209L;

	public void setTabLayoutPolicy(int scrollTabLayout) {
		super.setTabLayoutPolicy(scrollTabLayout);
	}

	public void actionPerformed(ActionEvent e) throws FileNotFoundException, JSONException {
		InfHandlerTable newTable = new InfHandlerTable();
		this.add(newTable.populateTable(e.getActionCommand()));
	}

	public InfHandlerTable addNewTable(String path) throws FileNotFoundException, JSONException {
		InfHandlerTable noviTable = new InfHandlerTable();
		return noviTable.populateTable(path);
	}

	public ArrayList<String> getChildTablePathList(String fileName) throws FileNotFoundException, JSONException {
		ArrayList<String> childData = new ArrayList<String>();
		String file = fileName.substring(0, fileName.length() - 4);
		File f = new File("data/Metaopisi");

		String adaptedJSONName = FileHelper.fileNametoCamelCase(file);
		File fileToSearchThrough = new File("data/Metaopisi/" + adaptedJSONName + ".json");

		if (f.exists()) {
			InputStream inputStream = new FileInputStream(fileToSearchThrough);
			Scanner s = new Scanner(inputStream).useDelimiter("\\A");
			String jsonTxt = s.hasNext() ? s.next() : "";
			JSONObject json = new JSONObject(jsonTxt);
			JSONObject relations = json.getJSONObject("relations");
			JSONArray to = relations.getJSONArray("to");
			for (int i = 0; i < to.length(); i++) {
				JSONObject objectInArray = to.getJSONObject(i);

				String[] elementNames = JSONObject.getNames(objectInArray);
				for (String elementName : elementNames) {
					String value = objectInArray.getString(elementName);
					if (elementName.equals("file")) {
						childData.add(value);
					}
				}
			}
		}
		return childData;
	}

	private ArrayList<InfHandlerTable> createChildTablesFromParent(ArrayList<String> pathList) throws FileNotFoundException, JSONException {
		ArrayList<InfHandlerTable> tables = new ArrayList<InfHandlerTable>();
		for (String path : pathList) {
			InfHandlerTable noviTable = new InfHandlerTable();
			noviTable.populateTable(path);
			tables.add(noviTable);
		}
		return tables;
	}

	public void setTabela(InfHandlerTable tabela, String naslov) {
		JScrollPane pomocni = new JScrollPane();
		pomocni.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pomocni.setViewportView(tabela);
		this.addTab(naslov, pomocni);
	}

	public void clearTabs() {
		this.removeAll();
	}

	public void openChildrenTabs(String parent) throws FileNotFoundException, JSONException {
		ArrayList<String> listaPotomaka = getChildTablePathList(parent);
		for (String potomak : listaPotomaka) {
			String []fileNameMembers = potomak.split("/");
			InfHandlerTable newTable = this.addNewTable(potomak);
			
			this.addTab(fileNameMembers[fileNameMembers.length-1],new JScrollPane(newTable));
			//setTabela(newTable, fileNameMembers[fileNameMembers.length-1]);
		}
	}

	public void openChildrenTabsDB(String parent) throws FileNotFoundException, JSONException {
		ArrayList<String> listaPotomaka = new ArrayList<String>();
		ArrayList<JTable> tableList = new ArrayList<JTable>();
		String JSONTableName = FileHelper.fileNametoCamelCase(parent);
		File f = new File("data/Metaopisi");
		File fileToSearchThrough = new File("data/Metaopisi/" + JSONTableName + ".json");

		if (f.exists()) {
			InputStream inputStream = new FileInputStream(fileToSearchThrough);
			Scanner s = new Scanner(inputStream).useDelimiter("\\A");
			String jsonTxt = s.hasNext() ? s.next() : "";
			JSONObject json = new JSONObject(jsonTxt);
			JSONObject relations = json.getJSONObject("relations");
			JSONArray to = relations.getJSONArray("to");
			for (int i = 0; i < to.length(); i++) {
				JSONObject objectInArray = to.getJSONObject(i);

				String[] elementNames = JSONObject.getNames(objectInArray);
				for (String elementName : elementNames) {
					String value = objectInArray.getString(elementName);
					if (elementName.equals("dbTable")) {
						listaPotomaka.add(value);
					}
				}
			}
		}
		
		for (String path : listaPotomaka) {
			//System.out.println(path);
			JTable newChildTable = DBHelper.dBTableToJTable(path);
			tableList.add(newChildTable);
			this.addTab(path, new JScrollPane(newChildTable));
		}
		
	}
}
