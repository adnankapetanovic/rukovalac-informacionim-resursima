package ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;

import org.json.JSONException;

public class TabbedInformationDisplay extends JTabbedPane{

	private static final long serialVersionUID = 1L;
	InfHandlerPalettePanel palettePanel = null;
	InfHandlerParentPalettePanel parentPalettePanel = null;
	InfHandlerChildPalettePanel childPalettePanel = null;
	
	public void setTabLayoutPolicy(int scrollTabLayout) {
		super.setTabLayoutPolicy(scrollTabLayout);
	}
	
	public void init() {
		this.add(getComponentPopupMenu());
		palettePanel = new InfHandlerPalettePanel();
		palettePanel.init();
	}
	
    public void actionPerformed(ActionEvent e) throws FileNotFoundException, JSONException {
    	InfHandlerTable newTable = new InfHandlerTable();
        this.add(newTable.populateTable(e.getActionCommand()));
    }
    
    public InfHandlerTable addNewTable(String path) throws FileNotFoundException, JSONException {
    	InfHandlerTable noviTable= new InfHandlerTable();
    	return noviTable.populateTable(path);
    }

	public void closeTab() {
		Component c = this.getSelectedComponent();
		this.remove(c);
	}
	
	public void setTabela(InfHandlerTable tabela, String naslov) {
		JScrollPane pomocni = new JScrollPane();
		pomocni.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pomocni.setViewportView(tabela);
		//this.add(pomocni);
		this.addTab(naslov, pomocni);
		
	}
}
