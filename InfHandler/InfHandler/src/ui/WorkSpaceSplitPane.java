package ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import helper.DBHelper;
import helper.FileHelper;

public class WorkSpaceSplitPane extends JSplitPane{
	public static InfHandlerDataSplitPane dataSplitPane = null;
	public static InfHandlerTablePanel tablePanel = null;
	public static JPanel auxPanel;
	public static JTree tree;
	
	public WorkSpaceSplitPane() {
		super();
	}

	public WorkSpaceSplitPane(int newOrientation, boolean newContinuousLayout, Component newLeftComponent,
			Component newRightComponent) {
		super(newOrientation, newContinuousLayout, newLeftComponent, newRightComponent);
	}

	public WorkSpaceSplitPane(int newOrientation, boolean newContinuousLayout) {
		super(newOrientation, newContinuousLayout);
	}

	public WorkSpaceSplitPane(int newOrientation, Component newLeftComponent, Component newRightComponent) {
		super(newOrientation, newLeftComponent, newRightComponent);
	}

	public WorkSpaceSplitPane(int newOrientation) {
		super(newOrientation);
	}
	
	protected void init() throws SQLException {
	    StringBuilder strPath = new StringBuilder();
	    
	    auxPanel = new JPanel(new BorderLayout());
	    
	    dataSplitPane = new InfHandlerDataSplitPane(JSplitPane.VERTICAL_SPLIT);
	    dataSplitPane.init();
	    tablePanel = new InfHandlerTablePanel(JSplitPane.VERTICAL_SPLIT);
	    tablePanel.init();
	    
	    this.add(dataSplitPane);
	    this.add(auxPanel);
	    //this.add(tablePanel);
	    tree = this.dataSplitPane.dataFileTree.tree;
	    tree.addMouseListener(new MouseAdapter() {
    		public void mouseClicked(MouseEvent e) {
    			if (e.getClickCount() == 2) {
    				DefaultMutableTreeNode node = (DefaultMutableTreeNode)
    						tree.getLastSelectedPathComponent();
    				if (node == null) return;
    				ArrayList<String> lista = new ArrayList<>();
    				TreeNode[] path = node.getPath();
    	        	
    	        	for (int i = 1; i<path.length; i++) {
    					strPath.append(path[i].toString());
    					lista.add(path[i].toString());
    				}
    	        	
    	        	String[] tempString = lista.get(0).split("\\\\");
    	        	String pathString = tempString[0] + "/" + tempString[1] + "/" + tempString[2] + "/" + lista.get(1);
    	        	String fileName = path[path.length-1].toString();
    	        	try {
    	        		if (auxPanel.getComponentCount() == 0) {
    	        			auxPanel.add(tablePanel, BorderLayout.CENTER);
						}
    	        		
						addNewTab(fileName, pathString);
					} catch (FileNotFoundException e1) {
						System.out.println("Fajl ne postoji.");;
					} catch (JSONException e1) {
						System.out.println("Dogodila se greska prilikom citanja JSON fajla.");
					}
    				} 
    			} 
    		});
	  //event za DB listu
	    this.dataSplitPane.displayList.addMouseListener(new MouseAdapter() {
	          public void mouseClicked(MouseEvent evt) {
	              JList list = (JList)evt.getSource();
	              if (evt.getClickCount() == 2) {

	                  // Double-click detected
	                  int index = list.locationToIndex(evt.getPoint());
	                  //System.out.println(index);
	                  //dobavi naziv tabele
	                  String tableName = (String) list.getSelectedValue();
	                  //System.out.println(tableName);
	                  String dbTableName = FileHelper.fileNametoCamelCase(tableName);
	                  //System.out.println(dbTableName);
	                  //pretoci dbtabelu u Jtabelu
	                  JTable newTable = DBHelper.dBTableToJTable(tableName);
	                  //prosledi tabelu na iscrtavanje
	                  try {
						addTableFromDB(tableName, newTable);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                  //provera da li postoji glavni split pane za podatke
	              }
	          }
	    });
	}
	public static ArrayList<String> openTabs = new ArrayList<String>(); 
	
	public static void addNewTab(String tabName, String path) throws FileNotFoundException, JSONException {

		ArrayList<String> tablePathList = tablePanel.tabbedChildPane.getChildTablePathList(tabName);
		int trazeniIndeks = 0;
		if (openTabs.contains(tabName)){
			int tabCount = tablePanel.tabbedPane.getTabCount();
			  for (int i=0; i < tabCount; i++) 
			  {
			    String tabTitle = tablePanel.tabbedPane.getTitleAt(i);
			    if (tabTitle.equals(tabName)) {
			    	trazeniIndeks = i;
			    }
			  }
			  tablePanel.tabbedPane.setSelectedIndex(trazeniIndeks);
		} else {
			openTabs.add(tabName);
			tablePanel.tabbedPane.add(tabName, new JScrollPane(tablePanel.addNewTab(path)));
			int selectedIndex = tablePanel.tabbedPane.getSelectedIndex();
            int nextIndex = tablePanel.tabbedPane.getTabCount()-1;
            tablePanel.tabbedPane.setSelectedIndex(nextIndex);
            
            if (tablePanel.tabbedChildPane.getTabCount() != 0) {
            	tablePanel.tabbedChildPane.removeAll();
			}
            
			for (String infHandlerTable : tablePathList) {
				String []fileNameMembers = infHandlerTable.split("/");
				tablePanel.tabbedChildPane.add(fileNameMembers[fileNameMembers.length-1],new JScrollPane(tablePanel.addNewTabC(infHandlerTable)));
			}
		}
		tablePanel.initContent();
	}
	public static void addTableFromDB(String tableName, JTable newTable) throws FileNotFoundException, JSONException {
		int trazeniIndeks = 0;
		String JSONTableName = FileHelper.fileNametoCamelCase(tableName);
		ArrayList<String> tablePathList = new ArrayList<String>();
		ArrayList<JTable> tableList = new ArrayList<JTable>();
		
		File f = new File("data/Metaopisi");
		File fileToSearchThrough = new File("data/Metaopisi/" + JSONTableName + ".json");

		if (f.exists()) {
			InputStream inputStream = new FileInputStream(fileToSearchThrough);
			Scanner s = new Scanner(inputStream).useDelimiter("\\A");
			String jsonTxt = s.hasNext() ? s.next() : "";
			JSONObject json = new JSONObject(jsonTxt);
			JSONObject relations = json.getJSONObject("relations");
			JSONArray to = relations.getJSONArray("to");
			for (int i = 0; i < to.length(); i++) {
				JSONObject objectInArray = to.getJSONObject(i);

				String[] elementNames = JSONObject.getNames(objectInArray);
				for (String elementName : elementNames) {
					String value = objectInArray.getString(elementName);
					if (elementName.equals("dbTable")) {
						tablePathList.add(value);
					}
				}
			}
		}
		if (openTabs.contains(tableName)){
			int tabCount = tablePanel.tabbedPane.getTabCount();
			  for (int i=0; i < tabCount; i++) 
			  {
			    String tabTitle = tablePanel.tabbedPane.getTitleAt(i);
			    if (tabTitle.equals(tableName)) {
			    	trazeniIndeks = i;
			    }
			  }
			  tablePanel.tabbedPane.setSelectedIndex(trazeniIndeks);
		} else {
			openTabs.add(tableName);
			tablePanel.tabbedPane.add(tableName, new JScrollPane(newTable));
			int selectedIndex = tablePanel.tabbedPane.getSelectedIndex();
            int nextIndex = tablePanel.tabbedPane.getTabCount()-1;
            tablePanel.tabbedPane.setSelectedIndex(nextIndex);
            
            if (tablePanel.tabbedChildPane.getTabCount() != 0) {
            	tablePanel.tabbedChildPane.removeAll();
			}
            
    		for (String path : tablePathList) {
    			//System.out.println(path);
    			JTable newChildTable = DBHelper.dBTableToJTable(tableName);
    			tableList.add(newChildTable);
    			tablePanel.tabbedChildPane.add(path, new JScrollPane(newChildTable));
    		}
		}
		tablePanel.initContent();
	}
	
	public Boolean proveriDaLiImaOtvorenihTabova() {
		if (this.tablePanel.tabbedPane.getTabCount() != 0) {
			return true;
		}
		return false;
	}
	
	public static ArrayList<String> dobaviOtvoreneTabove(){
		ArrayList<String> listaTabova = new ArrayList<String>();
		int brojOtvorenihTabova = tablePanel.tabbedPane.getTabCount();
		for (int i = 0; i < brojOtvorenihTabova; i++) {
			listaTabova.add(tablePanel.tabbedPane.getTitleAt(i));
			
		}
		return listaTabova;
	}

	public static void addTableFromDB2(String naziv) throws FileNotFoundException, JSONException {
		JTable newTable = DBHelper.dBTableToJTable(naziv);
		addTableFromDB(naziv, newTable);
	}
	
	public static void removeTablePanel() {
		auxPanel.remove(tablePanel);
		openTabs.clear();
	}
}
