## Naziv
Rukovalac informacionim resursima (InfHandler)

## Opis
Predmetni projekat iz specifikacije i modelovanja softvera.

## Instalacija
Da bi se projekat pokrenuo, potrebno je imati instaliran jdk 17.

## Upotreba
Aplikacija se izvršava pokretanjem App.java

## Autori
Autori projekta su Miloš Mrkšić (@milos.mrksic) i Adnan Kapetanović (@adnankapetanovic)

## Licenca
Ovo je projekat otvorenog koda. Dozvoljeno je preuzimanje i izmena istog.
